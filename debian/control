Source: fmtlib
Section: libdevel
Priority: optional
Maintainer: Raul Tambre <raul.tambre@clevon.com>
Homepage: https://fmt.dev/latest/
Vcs-Browser: https://gitlab.com/clevon/debian/fmt
Vcs-Git: https://gitlab.com/clevon/debian/fmt.git
Rules-Requires-Root: no
Standards-Version: 4.6.0
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-sequence-sphinxdoc <!nodoc>,
 doxygen <!nodoc>,
 ninja-build,
 node-less <!nodoc>,
 node-less-plugin-clean-css <!nodoc>,
 python3-breathe <!nodoc>,
 python3-sphinx <!nodoc>,

Package: libfmt-dev
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}
Suggests: libfmt-doc
Description: Modern C++ formatting library (development files)
 {fmt} is an open-source formatting library for C++. It can be used as a safe
 and fast alternative to (s)printf and iostreams.
 .
 This package contains the headers and the static library.

Package: libfmt-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: Modern C++ formatting library (documentation)
 {fmt} is an open-source formatting library for C++. It can be used as a safe
 and fast alternative to (s)printf and iostreams.
 .
 This package contains the API documentation.
